//package com.sber.java13Spring.java13SpringProject.helopdeskproject.controller;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.RoleDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Role;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.RoleService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/role")
//@Tag(name ="Роль пользователей в задаче", description = "Контроллер для работы с ролями пользователей")
//public class RoleController extends GenericController<Role, RoleDTO>{
//    private RoleService roleService;
//    public RoleController(RoleService roleService) {
//        super(roleService);
//    }
//}

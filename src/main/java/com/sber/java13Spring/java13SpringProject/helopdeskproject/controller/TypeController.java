//package com.sber.java13Spring.java13SpringProject.helopdeskproject.controller;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TypeDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Type;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.TypeService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/type")
//@Tag(name = "Тип заявки", description = "Контроллер для управдения типами заявки")
//public class TypeController extends GenericController<Type, TypeDTO>{
//    private TypeService typeService;
//    public TypeController(TypeService typeService) {
//        super(typeService);
//    }
//}

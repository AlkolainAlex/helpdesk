package com.sber.java13Spring.java13SpringProject.helopdeskproject.MVC.controller;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

// Model Viev Controller
@Controller
@Hidden
public class MainController {
//http://localhost:9090/
    @GetMapping("/")
    public String index() {
        return "index";
    }
}

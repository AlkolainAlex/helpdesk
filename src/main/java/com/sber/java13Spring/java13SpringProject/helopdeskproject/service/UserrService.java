package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.RoleDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.UserrDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.UserrMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Userr;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserrService
        extends GenericService<Userr, UserrDTO>{
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    protected UserrService(UserrRepository userrRepository,
                           UserrMapper userrMapper, BCryptPasswordEncoder bCryptPasswordEncoder) {
        super(userrRepository, userrMapper);
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    public UserrDTO getUserByLogin(final String login) {
        return mapper.toDTO(((UserrRepository) repository).findUserrByLogin(login));
    }
    public UserrDTO getUserByEmail(final String email) {
        return mapper.toDTO(((UserrRepository) repository).findUserrByEmail(email));
    }
    @Override
    public UserrDTO create(UserrDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
//        object.setCreatedBy("REGISTRATION FORM");
        object.setPassword(bCryptPasswordEncoder.encode(object.getPassword()));
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.GenericModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @param <T>
 */
@NoRepositoryBean
public interface GenericRepository<T extends GenericModel>
        extends JpaRepository<T, Long> {
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Status;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends GenericRepository<Status>{
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.StatusDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.StatusMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Status;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.StatusRepository;
import org.springframework.stereotype.Service;

@Service
public class StatusService
        extends GenericService<Status, StatusDTO>{
    protected StatusService(StatusRepository statusRepository,
                            StatusMapper statusMapper) {
        super(statusRepository, statusMapper);
    }
}

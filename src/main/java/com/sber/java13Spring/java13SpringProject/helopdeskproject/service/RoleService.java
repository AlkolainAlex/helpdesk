//package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.RoleDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.RoleMapper;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Role;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.RoleRepository;
//import org.springframework.stereotype.Service;
//
//@Service
//public class RoleService
//        extends GenericService<Role, RoleDTO>{
//    protected RoleService(RoleRepository roleRepository,
//                          RoleMapper roleMapper) {
//        super(roleRepository, roleMapper);
//    }
//}

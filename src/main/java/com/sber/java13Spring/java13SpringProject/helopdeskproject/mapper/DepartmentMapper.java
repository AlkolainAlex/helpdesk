package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.DepartmentDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Department;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class DepartmentMapper
        extends GenericMapper<Department, DepartmentDTO> {
    private final UserrRepository userrRepository;
    protected DepartmentMapper(ModelMapper mapper,
                               UserrRepository userrRepository) {
        super(mapper, Department.class, DepartmentDTO.class);
          this.userrRepository = userrRepository;
    }
    @Override
    protected void mapSpecificFields(DepartmentDTO source, Department destination) {
    }

    @Override
    protected void mapSpecificFields(Department source, DepartmentDTO destination) {
    }

    @Override
    protected Set<Long> getIds(Department entity) {
        return null;
    }
}

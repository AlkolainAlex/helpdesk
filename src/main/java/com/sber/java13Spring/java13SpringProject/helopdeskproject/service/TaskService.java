package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskWithUsersDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.TaskMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.TaskWithUserMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Task;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TaskService
        extends GenericService<Task, TaskDTO>{
    private final TaskRepository taskRepository;
    private final TaskWithUserMapper taskWithUserMapper;
    protected TaskService(TaskRepository taskRepository,
                          TaskMapper taskMapper,
                          TaskWithUserMapper taskWithUserMapper) {
        super(taskRepository,taskMapper);
        this.taskWithUserMapper = taskWithUserMapper;
        this.taskRepository = taskRepository;
    }
    public List<TaskWithUsersDTO> getAllTasksWithUsers() {
        log.info(taskWithUserMapper.toString());
        return taskWithUserMapper.toDTOs(taskRepository.findAll());
    }
}

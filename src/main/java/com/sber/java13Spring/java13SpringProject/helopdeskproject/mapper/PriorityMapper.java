package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.PriorityDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Priority;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class PriorityMapper extends GenericMapper<Priority, PriorityDTO> {
    private final TaskRepository taskRepository;
    protected PriorityMapper(ModelMapper mapper, TaskRepository taskRepository) {
        super(mapper, Priority.class, PriorityDTO.class);
        this.taskRepository = taskRepository;
    }

    @Override
    protected void mapSpecificFields(PriorityDTO source, Priority destination) {

    }

    @Override
    protected void mapSpecificFields(Priority source, PriorityDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Priority entity) {
        return null;
    }
}


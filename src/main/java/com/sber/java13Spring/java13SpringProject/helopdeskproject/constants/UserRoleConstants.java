package com.sber.java13Spring.java13SpringProject.helopdeskproject.constants;

public interface UserRoleConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String STP = "STP";
}

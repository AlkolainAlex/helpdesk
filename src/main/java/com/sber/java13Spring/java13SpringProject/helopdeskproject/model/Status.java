package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

// Статус
@Entity
@Table(name = "status")
@Getter
@Setter
//@NoArgsConstructor
//@SequenceGenerator(name = "default_generator", sequenceName = "status_seq",allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")

public class Status extends GenericModel{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long id;
//    Статус задачи

    @Column(name = "stat_task")
    private String statTask;
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TypeDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.TypeMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Type;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TypeRepository;
import org.springframework.stereotype.Service;

@Service
public class TypeService
        extends GenericService<Type, TypeDTO>{
    protected TypeService(TypeRepository typeRepository,
                          TypeMapper typeMapper) {
        super(typeRepository, typeMapper);

    }

}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

// Приоритет
@Entity
@Table(name = "priority")
@Getter
@Setter
//@NoArgsConstructor
//@SequenceGenerator(name = "default_generator", sequenceName = "priority_seq",allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")

public class Priority extends GenericModel{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long id;
//  Типы приоритетов
    @Column(name = "type_priority")
    private String typePrior;
}

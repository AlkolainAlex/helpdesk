package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Department;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends GenericRepository<Department>{
}

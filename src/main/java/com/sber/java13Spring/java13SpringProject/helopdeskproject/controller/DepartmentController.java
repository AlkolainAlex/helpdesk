//package com.sber.java13Spring.java13SpringProject.helopdeskproject.controller;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.DepartmentDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Department;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.DepartmentService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/department")
//@Tag(name = "Управление/подразделение", description = "Контроллер для работы с Управлениями/подразделениями")
//public class DepartmentController extends GenericController<Department, DepartmentDTO>{
//    private DepartmentService departmentService;
//    public DepartmentController(DepartmentService departmentService) {
//        super(departmentService);
//    }
//}

//package com.sber.java13Spring.java13SpringProject.helopdeskproject.controller;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.StatusDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Status;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.StatusService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/status")
//@Tag(name = "Статус заявки", description = "Контроллер для работы со статусом")
//public class StatusController extends GenericController<Status, StatusDTO>{
//    private StatusService statusService;
//    public StatusController(StatusService statusService) {
//        super(statusService);
//    }
//}

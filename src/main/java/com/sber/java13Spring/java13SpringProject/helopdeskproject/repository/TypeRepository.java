package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Type;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeRepository extends GenericRepository<Type>{
}

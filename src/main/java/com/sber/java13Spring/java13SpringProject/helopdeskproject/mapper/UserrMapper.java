package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.UserrDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.GenericModel;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Userr;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TaskRepository;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class UserrMapper extends GenericMapper<Userr, UserrDTO>{
    private TaskRepository taskRepository;
    protected UserrMapper(ModelMapper modelMapper, TaskRepository taskRepository) {
        super(modelMapper, Userr.class, UserrDTO.class);
        this.taskRepository = taskRepository;
    }
    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Userr.class, UserrDTO.class)
                .addMappings(m -> m.skip(UserrDTO::setTask_id)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserrDTO.class, Userr.class)
                .addMappings(m -> m.skip(Userr::setTasks)).setPostConverter(toEntityConverter());
//                .addMappings(m->m.skip(Userr::setBirthDate)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(UserrDTO source, Userr destination) {
        if (!Objects.isNull(source.getTask_id()) && source.getTask_id().size() > 0) {
            destination.setTasks(new HashSet<>(taskRepository.findAllById(source.getTask_id())));
        } else {
            destination.setTasks(Collections.emptySet());
        }
//        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()).toString());
    }

    @Override
    protected void mapSpecificFields(Userr source, UserrDTO destination) {
        destination.setTask_id(getIds(source));
    }
    @Override
    protected Set<Long> getIds(Userr user) {
        return  Objects.isNull(user)|| Objects.isNull(user.getTasks())
                ? null
                : user.getTasks().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

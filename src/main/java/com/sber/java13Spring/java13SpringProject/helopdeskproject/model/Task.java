package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

// Заявка
@Entity
@Table(name = "task")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "task_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Task extends GenericModel{
// Тип заявки
    @ManyToOne
    @JoinColumn(name = "type_id", foreignKey = @ForeignKey(name = "FK_TASK_TYPE"))
    private Type type;
// Приоритет
    @ManyToOne
    @JoinColumn(name = "priority_id", foreignKey = @ForeignKey(name = "FK_TASK_PRIORITY"))
    private Priority priority;
// Стату заявки
    @ManyToOne
    @JoinColumn(name = "status_id", foreignKey = @ForeignKey(name = "FK_TASK_STATUS"))
    private Status status;
// Время закрытия
    @Column(name = "time_clozed")
    private LocalDateTime time_clozed;
// Комментарий
    @Column(name = "comment")
    private String comment;
// Описание
    @Column(name = "description", nullable = false)
    private String description;
// Дополнение
    @Column(name = "addition")
    private String addition;
// список пользователей относящихся к задаче
    @ManyToMany
    @JoinTable(name = "task_userr",
                joinColumns = @JoinColumn(name = "task_id"), foreignKey = @ForeignKey(name = "FK_TASK_USERR"),
                inverseJoinColumns = @JoinColumn(name = "userr_id"), inverseForeignKey = @ForeignKey(name = "FK_USER_TASK"))
    private Set<Userr> userrs;
}

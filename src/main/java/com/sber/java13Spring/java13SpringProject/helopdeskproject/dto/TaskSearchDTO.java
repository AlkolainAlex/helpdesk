package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskSearchDTO {
    private String taskType;
    private String taskPriority;
    private String taskStatus;
}

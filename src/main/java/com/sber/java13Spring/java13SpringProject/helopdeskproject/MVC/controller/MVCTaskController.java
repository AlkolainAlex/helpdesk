package com.sber.java13Spring.java13SpringProject.helopdeskproject.MVC.controller;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskWithUsersDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.TaskService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Hidden
@Controller
@RequestMapping("/tasks")
@Slf4j
public class MVCTaskController {
    private final TaskService  taskService;
    public MVCTaskController(TaskService taskService) {
        this.taskService = taskService;
    }
    @GetMapping("")
    public String getAll(Model model) {
        List<TaskWithUsersDTO> taskWithUsersDTOList = taskService.getAllTasksWithUsers();
        model.addAttribute("tasks", taskWithUsersDTOList);
        log.info(String.valueOf(taskWithUsersDTOList));
        log.info(model.toString());
        return "tasks/viewAllTasks";
    }

    @GetMapping("/add")
    public String create() {
        return "tasks/addTask";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("taskForm") TaskDTO taskDTO) {
                   taskService.create(taskDTO/*, file*/);
             return "redirect:/books";
    }

//    @PostMapping
//    public String searchTasks(@ModelAttribute("taskSearchForm")TaskSearchDTO taskSearchDTO, Model model) {
//        model.addAttribute("tasks", taskService.findTasks());
//        return "tasks/viewAllTasks";
//
//    }

}

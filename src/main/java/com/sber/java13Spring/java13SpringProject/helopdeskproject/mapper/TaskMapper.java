package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskWithUsersDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.GenericModel;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Task;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.PriorityRepository;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.StatusRepository;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TypeRepository;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TaskMapper extends GenericMapper<Task, TaskDTO>{
    private final StatusRepository statusRepository;
    private final TypeRepository typeRepository;
    private final PriorityRepository priorityRepository;
    private final UserrRepository userrRepository;
    protected TaskMapper(ModelMapper modelMapper,
                         UserrRepository userrRepository,
                         StatusRepository statusRepository,
                         TypeRepository typeRepository,
                         PriorityRepository priorityRepository) {
        super(modelMapper, Task.class, TaskDTO.class);
        this.userrRepository = userrRepository;
        this.statusRepository = statusRepository;
        this.typeRepository = typeRepository;
        this.priorityRepository = priorityRepository;
    }

    @PostConstruct
    public void setupMapper() {
        modelMapper.createTypeMap(Task.class, TaskDTO.class)
                .addMappings(m -> m.skip(TaskDTO::setUserr_id)).setPostConverter(toDTOConverter())
                .addMappings(m->m.skip(TaskDTO::setStatus_id)).setPostConverter(toDTOConverter())
                .addMappings(m->m.skip(TaskDTO::setPriority_id)).setPostConverter(toDTOConverter())
                .addMappings(m->m.skip(TaskDTO::setType_id)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(TaskDTO.class, Task.class)
                .addMappings(m -> m.skip(Task::setUserrs)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Task::setStatus)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Task::setPriority)).setPostConverter(toEntityConverter())
                .addMappings(m->m.skip(Task::setType)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(TaskDTO source, Task destination) {
        if (!Objects.isNull(source.getUserr_id()) && source.getUserr_id().size() > 0) {
            destination.setUserrs(new HashSet<>(userrRepository.findAllById(source.getUserr_id())));
        } else {
            destination.setUserrs(Collections.emptySet());
        }
        destination.setStatus(statusRepository.findById(source.getStatus_id()).orElseThrow(()->new NotFoundException("Такой статус не найден")));
        destination.setPriority(priorityRepository.findById(source.getPriority_id()).orElseThrow(()->new NotFoundException("Такой приоритет не найден")));
        destination.setType(typeRepository.findById(source.getType_id()).orElseThrow(()->new NotFoundException("Такой тип не найден")));

    }

    @Override
    protected void mapSpecificFields(Task source, TaskDTO destination) {
        destination.setUserr_id(getIds(source));
        destination.setStatus_id(source.getStatus().getId());
        destination.setPriority_id(source.getPriority().getId());
        destination.setType_id(source.getType().getId());

    }
    protected  Set<Long> getIds(Task task) {
        return  Objects.isNull(task)|| Objects.isNull(task.getUserrs())
                ? null
                : task.getUserrs().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

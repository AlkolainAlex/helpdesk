//package com.sber.java13Spring.java13SpringProject.helopdeskproject.controller;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.PriorityDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Priority;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.PriorityService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/priority")
//@Tag(name = "Приоритет", description = "Контроллер для работы с приоритетом")
//public class PriorityController extends GenericController<Priority, PriorityDTO> {
//    private PriorityService priorityService;
//    public PriorityController(PriorityService priorityService) {
//        super(priorityService);
//    }
//}

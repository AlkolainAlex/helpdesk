package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

// Роль полльзователя в задаче
@Entity
@Table(name = "role")
@Getter
@Setter

public class Role {
// Наименование роли
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;

}

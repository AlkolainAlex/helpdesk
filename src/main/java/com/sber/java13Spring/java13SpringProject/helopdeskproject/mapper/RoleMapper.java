//package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.RoleDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Role;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.RoleRepository;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
//import org.modelmapper.ModelMapper;
//import org.springframework.stereotype.Component;
//
//@Component
//public class RoleMapper extends GenericMapper<Role, RoleDTO> {
//    private final UserrRepository userrRepository;
//    protected RoleMapper(ModelMapper mapper, UserrRepository userrRepository) {
//        super(mapper,Role.class, RoleDTO.class);
//        this.userrRepository = userrRepository;
//    }
//
//    @Override
//    void mapSpecificFields(RoleDTO source, Role destination) {
//
//    }
//
//    @Override
//    void mapSpecificFields(Role source, RoleDTO destination) {
//
//    }
//}

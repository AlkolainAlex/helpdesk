package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Task;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Userr;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Slf4j
public class TaskWithUsersDTO extends TaskDTO{
    public TaskWithUsersDTO(Task task, Set<UserrDTO> userrs) {
        super(task);
        this.userrs = userrs;
    }
    private Set<UserrDTO> userrs;
    public void mainuser(UserrDTO users) {
        log.info(users.toString());
    }
}

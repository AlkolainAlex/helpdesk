package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RoleDTO {
    private Long id;
    private String title;
    private String description;
}

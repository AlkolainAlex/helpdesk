package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.StatusDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Status;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class StatusMapper extends GenericMapper<Status, StatusDTO> {
    private final UserrRepository userrRepository;
    protected  StatusMapper(ModelMapper mapper, UserrRepository userrRepository) {
        super(mapper, Status.class, StatusDTO.class);
        this.userrRepository = userrRepository;
    }

    @Override
    protected void mapSpecificFields(StatusDTO source, Status destination) {
    }

    @Override
    protected void mapSpecificFields(Status source, StatusDTO destination) {
    }

    @Override
    protected Set<Long> getIds(Status entity) {
        return null;
    }
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDTO extends GenericDTO{
    private Long depID;
    private Long subID;
    private String nameDepSub;
}

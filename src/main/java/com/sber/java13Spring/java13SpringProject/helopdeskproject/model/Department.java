package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

// Управление Подразделение
@Entity
@Table(name = "department")
@Getter
@Setter

public class Department extends GenericModel{
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private Long id;
//  Управление
    @Column(name = "dep_id")
    private Long depID;
//  Подразделение
    @Column(name = "sub_id", nullable = false)
    private Long subID;
// наименование Управления или подразделения
    @Column(name = "name_depart_subvis", nullable = false)
    private String nameDepSub;
}

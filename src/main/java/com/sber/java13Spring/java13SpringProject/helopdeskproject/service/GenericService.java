package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.GenericDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.GenericMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.GenericModel;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

/**
 * Абстрактный сервис, который хранит в себе реализацию CRUD операций по-умолчанию.
 * Если реализация отличная от того, что представлено в этом классе,
 * то она переопределяется в реализации конкретного сервиса.
 *
 * @param <T> - Сущность, с которой мы работаем.
 * @param <N> - DTO, которую мы будем отдавать/принимать дальше.
 */

@Service
public abstract class GenericService<T extends GenericModel,
        N extends GenericDTO>{
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;
    public GenericService(GenericRepository<T> repository, GenericMapper<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }
    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }
    public N getOne(final Long id) {
        return mapper.toDTO(repository.findById(id)
                .orElseThrow(() -> new NotFoundException("Данных для id : " + id + " не найдено.")));
    }

    public N create(N newObject){return mapper.toDTO(repository.save(mapper.toEntity(newObject)));}
    public N update(N updateObject){return mapper.toDTO(repository.save(mapper.toEntity(updateObject)));}
    public void delete(final Long id){repository.deleteById(id);}
}

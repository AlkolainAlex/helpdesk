package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Slf4j
public class UserrDTO extends GenericDTO {
    private String login;
    private String password;
    private String email;
//    private String birthDate;
    private String surname;
    private String name;
    private String middleName;
    private Long department_id;
    private String kab;
    private String jobTitle;
    private RoleDTO role;
    private Set<Long> task_id;
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.service.userdetails;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.constants.UserRoleConstants;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Userr;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService
        implements UserDetailsService {
    @Value("${spring.security.user.name}")
    private String adminUserName;
    @Value("${spring.security.user.password}")
    private String adminPassword;
    @Value("${spring.security.user.roles}")
    private String adminRole;

    private final UserrRepository userrRepository;
    public CustomUserDetailsService(UserrRepository userrRepository) {
        this.userrRepository = userrRepository;
    }
// проверка пользователя, TODO: протестировать LDAP
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.equals(adminUserName)) {
            return new CustomUserDetails(null,
                    username,
                    adminPassword,
                    List.of(new SimpleGrantedAuthority("ROLE_" + adminRole)));
        } else {
            Userr user = userrRepository.findUserrByLogin(username);
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(user.getRole().getId() == 1L ? "ROLE_" + UserRoleConstants.USER :
                            "ROLE_" + UserRoleConstants.STP));
            return new CustomUserDetails(user.getId().intValue(), username, user.getPassword(), authorities);
        }
    }
}

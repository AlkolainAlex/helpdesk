package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Userr;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserrRepository extends GenericRepository<Userr>{
//  При сложных запросах в талицу, можно писать их через @Query
//    @Query(nativeQuery = true, value = "select * from userr where login = :login")
    Userr findUserrByLogin(String login);
    Userr findUserrByEmail(String email);
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TypeDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Type;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.TaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class TypeMapper extends GenericMapper<Type, TypeDTO>{
    private final TaskRepository taskRepository;
    protected TypeMapper(ModelMapper modelMapper,TaskRepository taskRepository) {
        super(modelMapper, Type.class, TypeDTO.class);
        this.taskRepository = taskRepository;
    }

    @Override
    protected void mapSpecificFields(TypeDTO source, Type destination) {

    }

    @Override
    protected void mapSpecificFields(Type source, TypeDTO destination) {

    }

    @Override
    protected Set<Long> getIds(Type entity) {
        return null;
    }
}

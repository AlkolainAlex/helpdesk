package com.sber.java13Spring.java13SpringProject.helopdeskproject.repository;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Priority;
import org.springframework.stereotype.Repository;

@Repository
public interface PriorityRepository extends GenericRepository<Priority>{
}

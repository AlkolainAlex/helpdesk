package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.DepartmentDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.DepartmentMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Department;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

@Service
public class DepartmentService
        extends GenericService<Department, DepartmentDTO>{
    protected DepartmentService(DepartmentRepository departmentRepository,
                                DepartmentMapper departmentMapper) {
        super(departmentRepository, departmentMapper);
    }
}

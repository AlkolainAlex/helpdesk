package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PriorityDTO extends GenericDTO{
    private String typePrior;
}

package com.sber.java13Spring.java13SpringProject.helopdeskproject.service;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.PriorityDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper.PriorityMapper;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Priority;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.PriorityRepository;
import org.springframework.stereotype.Service;

@Service
public class PriorityService extends GenericService<Priority, PriorityDTO>{
    protected PriorityRepository priorityRepository;
    protected PriorityService(PriorityRepository priorityRepository, PriorityMapper priorityMapper) {
        super(priorityRepository, priorityMapper);
        this.priorityRepository = priorityRepository;
    }
}

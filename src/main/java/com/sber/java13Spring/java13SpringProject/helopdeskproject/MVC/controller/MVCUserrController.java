package com.sber.java13Spring.java13SpringProject.helopdeskproject.MVC.controller;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.UserrDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.service.UserrService;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.sber.java13Spring.java13SpringProject.helopdeskproject.constants.UserRoleConstants.ADMIN;

@Controller
@Hidden
@RequestMapping("/users")
@Slf4j
public class MVCUserrController {
    private final UserrService userrService;
    public MVCUserrController(UserrService userrService) {
        this.userrService = userrService;
    }
    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserrDTO());
        return "registration";
    }
    @PostMapping("/geristration")
    public String registration(@ModelAttribute("userForm") UserrDTO userrDTO, BindingResult bindingResult) {
        if (userrDTO.getLogin().equalsIgnoreCase(ADMIN) || userrService.getUserByLogin(userrDTO.getLogin()) != null) {
            bindingResult.rejectValue("login", "error.login", "Такой логин уже существует");
            return "registration";
        }
        if (userrService.getUserByEmail(userrDTO.getLogin()) != null) {
            bindingResult.rejectValue("email", "error.email", "Такой email уже существует");
            return "registration";
        }
        userrService.create(userrDTO);
        return "redirect:login";
    }
}

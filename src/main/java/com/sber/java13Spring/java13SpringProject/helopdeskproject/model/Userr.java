package com.sber.java13Spring.java13SpringProject.helopdeskproject.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

// Пользователи
@Entity
@Table(name = "userr",/* имя таблицы и указания уникальных полей */
        uniqueConstraints = {@UniqueConstraint(name = "uniqueEmail", columnNames = "email"),
                @UniqueConstraint(name = "uniqueLogin", columnNames = "login")})

@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "userr_seq",allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
@ToString
public class Userr extends GenericModel {
    @Column(name = "login", nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "email", nullable = false)
    private String email;
// Фамилия
    @Column(name = "surname", nullable = false)
    private String surname;
// Имя
    @Column(name = "name", nullable = false)
    private String name;
// Отчество
    @Column(name = "middle_name")
    private String middleName;
//    @Column(name = "birthDate", nullable = false)
//    private String birthDate;
// Управление, подразделение
    @ManyToOne
    @JoinColumn(name = "department_id", foreignKey = @ForeignKey(name = "FK_DEPARTMENT_USERR"))
    private Department department;
// Кабинет
    @Column(name = "kabinet")
    private String kab;
// Должность
    @Column(name = "job_title")
    private String jobTitle;
// Роль в задаче (постановщик, исполнитель, наблюдающий)
    @ManyToOne
    @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "FK_ROLE_USERR"))
    private Role role;
// Список задач пользователя
    @ManyToMany
    @JoinTable(name = "task_userr",
            joinColumns = @JoinColumn(name = "userr_id"), foreignKey = @ForeignKey(name = "FK_USERR_TASK"),
            inverseJoinColumns = @JoinColumn(name = "task_id"), inverseForeignKey = @ForeignKey(name = "FK_TASK_USERR"))
    private Set<Task> tasks;
}

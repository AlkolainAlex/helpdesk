package com.sber.java13Spring.java13SpringProject.helopdeskproject.dto;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO extends GenericDTO{
    private Long type_id;
    private Long priority_id;
    private Long status_id;
    private LocalDateTime time_clozed;
    private String comment;
    private String description;
    private String addition;
    private Set<Long> userr_id;

    public TaskDTO(Task task) {
        TaskDTO taskDTO = new TaskDTO();
        Type type = task.getType();
        Priority priority = task.getPriority();
        Status status = task.getStatus();
        taskDTO.setTime_clozed(task.getTime_clozed());
        taskDTO.setComment(task.getComment());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setAddition(task.getAddition());
        Set<Userr> userrs = task.getUserrs();
        Set<Long> userr_id = new HashSet<>();
        if (userrs != null && userrs.size() > 0) {
            userrs.forEach(a->userr_id.add(a.getId()));
        }
        taskDTO.setUserr_id(userr_id);
    }
}

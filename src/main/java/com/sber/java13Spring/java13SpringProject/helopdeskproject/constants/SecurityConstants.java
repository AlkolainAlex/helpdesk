package com.sber.java13Spring.java13SpringProject.helopdeskproject.constants;

import java.util.List;

public interface SecurityConstants {
    List<String> RESOURCES_WHITE_LIST = List.of
            ("/resources/**",
                    "/js/**",
                    "/css/**",
                    "/swagger-ui/**",
                    "/error",
                    "/v3/api-docs/**",
                    "/webjars/bootstrap/5.0.2/**",
                    "/");
    List<String> TASKS_WHITE_LIST = List.of("/tasks");
    //TODO (/tasks);
    List<String> TASKS_PERMISSION_LIST =List.of
            ("/tasks/add",
                    "/tasks/update",
                    "/tasks/update");
    List<String> USERS_WHITE_LIST = List.of
            ("/login",
                    "/users/registration",
                    "/users/remember-password",
                    "/users/change-password");
}

//package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;
//
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskWithStatusDTO;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Task;
//import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.StatusRepository;
//import jakarta.annotation.PostConstruct;
//import org.modelmapper.ModelMapper;
//import org.springframework.stereotype.Component;
//import org.webjars.NotFoundException;
//
//import java.util.Set;
//
//@Component
//public class TaskWithStatusMapper extends GenericMapper<Task, TaskWithStatusDTO>{
//    private final StatusRepository statusRepository;
//    protected TaskWithStatusMapper(ModelMapper modelMapper,
//                                   StatusRepository statusRepository) {
//        super(modelMapper, Task.class, TaskWithStatusDTO.class);
//        this.statusRepository = statusRepository;
//    }
//
//    @PostConstruct
//    protected void setupMapper() {
//        modelMapper.createTypeMap(Task.class, TaskWithStatusDTO.class)
//                .addMappings(m->m.skip(TaskWithStatusDTO::setStatus_id)).setPostConverter(toDTOConverter());
//        modelMapper.createTypeMap(TaskWithStatusDTO.class, Task.class)
//                .addMappings(m->m.skip(Task::setStatus)).setPostConverter(toEntityConverter());
//    }
//    @Override
//    protected void mapSpecificFields(TaskWithStatusDTO source, Task destination) {
//        destination.setStatus(statusRepository.findById(source.getStatus_id()).orElseThrow(()->new NotFoundException("Такой статус не найден")));
//    }
//
//    @Override
//    protected void mapSpecificFields(Task source, TaskWithStatusDTO destination) {
//        destination.setStatus_id(source.getStatus().getId());
//    }
//
//    @Override
//    protected Set<Long> getIds(Task entity) {
//        throw new UnsupportedOperationException("Метод недоступен");
////        return null;
//    }
//}

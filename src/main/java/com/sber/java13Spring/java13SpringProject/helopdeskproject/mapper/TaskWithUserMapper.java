package com.sber.java13Spring.java13SpringProject.helopdeskproject.mapper;

import com.sber.java13Spring.java13SpringProject.helopdeskproject.dto.TaskWithUsersDTO;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.GenericModel;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.model.Task;
import com.sber.java13Spring.java13SpringProject.helopdeskproject.repository.UserrRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class TaskWithUserMapper extends GenericMapper<Task, TaskWithUsersDTO> {
    private final UserrRepository userrRepository;
    protected TaskWithUserMapper(ModelMapper modelMapper,
                                 UserrRepository userrRepository) {
        super(modelMapper, Task.class, TaskWithUsersDTO.class);
        this.userrRepository = userrRepository;
    }
    @PostConstruct
    protected void setupMapper() {
        modelMapper.createTypeMap(Task.class, TaskWithUsersDTO.class)
                .addMappings(m->m.skip(TaskWithUsersDTO::setUserr_id)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(TaskWithUsersDTO.class, Task.class)
                .addMappings(m->m.skip(Task::setUserrs)).setPostConverter(toEntityConverter());
    }
    @Override
    protected void mapSpecificFields(TaskWithUsersDTO source, Task destination) {
        destination.setUserrs(new HashSet<>(userrRepository.findAllById(source.getUserr_id())));
    }
    @Override
    protected void mapSpecificFields(Task source, TaskWithUsersDTO destination) {
        destination.setUserr_id(getIds(source));
    }
    @Override
    protected Set<Long> getIds(Task entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getId())
                ? null
                :entity.getUserrs().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
